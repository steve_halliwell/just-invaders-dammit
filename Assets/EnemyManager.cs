﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour {

	public Transform startPoint;
	public Vector2 dims;
	public Vector2 step;
	public GameObject enemyPre;
	
	
	public float fireRateFullRanks, fireRateEmptyRanks;
	private float enemyFireCounter = 0;
	
	public bool hasHitSide = false;

	public float direction = 1;
	public float speed = 1;
	public float dropHeight = 1;

	public List<GameObject> enemies = new List<GameObject>();

	void Start()
	{
		for(int i = 0; i < dims.y; i++)
		{
			for(int j = 0; j < dims.x; j++)
			{
				Vector3 newPos = startPoint.position + new Vector3(step.x * j, step.y * -i,0);
				GameObject newEnemy = (GameObject) Instantiate(enemyPre, 
				                                               newPos,
				                                               Quaternion.identity);

				newEnemy.transform.parent = transform;
				enemies.Add(newEnemy);
			}
		}
	}

	void FixedUpdate()
	{
		hasHitSide = false;

		transform.Translate (speed * direction * Time.deltaTime, 0, 0);
		
		//make a random one fire at given rate
		enemyFireCounter += Time.deltaTime;
		
		//print ((enemyFireRate * (enemies.Count / enemyFireScale)) );
		float timeTilFire = (fireRateFullRanks - fireRateEmptyRanks) * (enemies.Count / (dims.x * dims.y) ) + fireRateEmptyRanks;
		
		if(enemyFireCounter >= timeTilFire )
		{
			enemies[Random.Range(0, enemies.Count)].SendMessage("OnFireBullet");
			enemyFireCounter = 0;
		}
	}
	
	void OnHitSide()
	{
		if(!hasHitSide)
		{
			transform.Translate(0,-dropHeight,0);
			direction = direction *-1;
		}

		hasHitSide = true;
	}
	
	void OnChildDie(GameObject go)
	{
		enemies.Remove(go);
		if(enemies.Count == 0)
		{
			// player win
			Application.LoadLevel("GameOverWin");
		}
	}
}
