﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	void Start()
	{
		Destroy (gameObject, 10);
	}

	void OnDie()
	{
		gameObject.SetActive (false);
		Destroy (gameObject);
	}
}
