﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public GameObject bulletPre;
	public float bulletSpeed = 1;

	void OnDie()
	{
		gameObject.SetActive (false);
		SendMessageUpwards("OnChildDie", gameObject);
		Destroy (gameObject);
	}	
	
	void OnFireBullet()
	{
		GameObject newBullet = (GameObject)Instantiate(bulletPre);
		
		newBullet.transform.position = transform.position;
		
		newBullet.tag = "EnemyBullet";
		newBullet.rigidbody2D.velocity = new Vector2(0,-bulletSpeed);
	}
}
