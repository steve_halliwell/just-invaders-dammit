﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public float speed;
	public GameObject bulletPre;
	public float fireRate = 1;
	private float counter = 0;
	public float bulletSpeed = 1;
	public int lives = 3;
	public float stageWidth;
	
	private Vector3 startPos;
	
	void Start()
	{
		startPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		transform.Translate (Input.GetAxis ("Horizontal") * speed * Time.deltaTime, 0, 0);
		counter += Time.deltaTime;

		if(counter >= fireRate && Input.GetKey("space"))
		{
			GameObject newBullet = (GameObject)Instantiate(bulletPre);

			newBullet.transform.position = transform.position;

			newBullet.tag = "PlayerBullet";
			newBullet.rigidbody2D.velocity = new Vector2(0,bulletSpeed);

			counter = 0;
		}

		float curx = transform.position.x;
		float dif = 0;
		
		if(Mathf.Abs(curx) > stageWidth)
		{
			dif = stageWidth * Mathf.Sign(curx) - curx;
		}
		
		transform.Translate(dif, 0,0);
		
	}

	void OnDie()
	{
		lives--;
		
		if(lives <= 0)
		{
			Application.LoadLevel ("GameOverFail");
		}
		else
		{
			//reset pos
			transform.position = startPos;
		}
	}

}
