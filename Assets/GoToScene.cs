﻿using UnityEngine;
using System.Collections;

public class GoToScene : MonoBehaviour {

	public KeyCode key;
	public string sceneName;

	void Update()
	{
		if(Input.GetKeyDown(key))
		{
			Application.LoadLevel(sceneName);
		}
	}
}
