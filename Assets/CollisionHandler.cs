﻿using UnityEngine;
using System.Collections;

public class CollisionHandler : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col == null)
			return;
			
		//player hit enemy bullet die
		if(tag == "PlayerBullet" && col.tag == "Enemy")
		{
			col.SendMessage("OnDie");
			SendMessage("OnDie");
		}

		//enemy hit player bullet die
		else if(tag == "EnemyBullet" && col.tag == "Player")
		{
			col.SendMessage("OnDie");
			SendMessage("OnDie");
		}
		
		else if((tag == "EnemyBullet" || tag == "PlayerBullet") && col.tag == "Barrier")
		{
			col.SendMessage("OnDie");
			SendMessage("OnDie");
		}

		//wall and enemy
       	else if(tag == "Enemy" && col.tag == "Wall")
		{
			SendMessageUpwards("OnHitSide");
		}

		else if(tag == "Enemy" && col.tag == "Respawn")
		{
			// player lose
			Application.LoadLevel("GameOverFail");
		}
	}
}
