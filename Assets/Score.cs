﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	public int enemiesKilled = 0;
	public Player player;
	
	void OnGUI()
	{
		GUI.Label ( new Rect(10,10,300,30), "Enemies killed: " + enemiesKilled.ToString());
		GUI.Label ( new Rect(10,20,300,30), "Lives remaining: " + player.lives.ToString());
	}

}
